<p align="center"><a href="https://laravel.com" target="_blank">
<img src="https://files.realpython.com/media/Python-Docker-Tutorials_Watermarked.f9834dc9df9a.jpg" width="200"></a></p>


## About This project

The main goal of this project, except showing off some features of Laravel, is implementation
of something similar to Portainer (another Saas project for provisioning, written in GoLang)

###What it should do:

- Allow registered users to monitor, pause/stop/destroy running containers or create a new one
- Requests are performed through HTTP client by using standard Docker unix socket (for real production it might be a
better idea, like opening TCP transport and limited network between app and daemon host)
- Some task can be delayed, therefore queued
- It all works on custom docker composition (yes, we have sail now, but
I get used to doing all by myself without trust to something completely 
done for me)
- For some reason there is fake SQS run, but it's not debugged as it should
 be, so queues now go to Redis channel (you can watch them through admin panel on localhost:5001, host *redis*, 
but also through artisan as well), but basically this command was never used
  ![Scheme](docs/screenshots/artisan.png)
- It uses memcached as a session driver, just for fun
- The main image is my custom one, PHP 7.4.9, but it wasn't updated recently, it just has all PDO and everything, but
I should also update composer, Xdebug to version 3, and session driver, then pushed back to prebuilt image in hub

![Scheme](docs/screenshots/screenshot-1.png)

![Scheme](docs/screenshots/refresh.gif)

![Scheme](docs/screenshots/journal.png)

But, anyways, for starting this thing you just need Docker, run ***docker-compose up -d*** and then ***docker exec -it FPM sh -c "composer i"***
P.S.: queues run and work in separate "Cron" container
UPDATE: now it's Nginx + FPM setup
