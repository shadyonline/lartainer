<?php

use App\Http\Controllers\ContainerController;
use App\Http\Middleware\EnsureTokenIsValid;
use App\Http\Middleware\EnsureUserIsPresent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('web')->group(function () {
    Route::resource('/containers', ContainerController::class)->except([
        'create', 'edit', 'update'
    ]);
    Route::post('/containers/{id}/start', [ContainerController::class, 'start'])->name('start');
    Route::post('/containers/{id}/pause', [ContainerController::class, 'pause'])->name('pause');
    Route::post('/containers/{id}/stop', [ContainerController::class, 'stop'])->name('stop');
});
