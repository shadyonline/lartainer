<?php

use App\Models\Journal;
use Aws\Credentials\CredentialProvider;
use Aws\Sqs\SqsClient;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return redirect()->route('dashboard');
    }
    return redirect()->route('register');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');
Route::get('/journal', function () {
    return view('journal', ['entries' => Journal::all()->reverse()->toArray()]);
})->middleware(['auth'])->name('journal');

require __DIR__.'/auth.php';
