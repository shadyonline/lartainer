<?php

namespace App\Services;

use App\Models\Journal;
use App\Models\User;
use App\Notifications\ActionPerformed;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Request;

class ContainerService
{
    private PendingRequest $gateway;
    private Request $request;

    public function __construct(Request $request)
    {
        $version = app()->config->get('services.docker.version');
        $this->gateway = Http::withOptions(
            [
                'curl' => [
                    CURLOPT_UNIX_SOCKET_PATH => '/var/run/docker.sock'
                ]
            ]
        )->baseUrl("http:/v$version")
            ->async(true);
        $this->request = $request;
    }

    public function list(): array
    {
        return json_decode(
            $this->gateway
                ->get('containers/json?all=1')
                ->then(function ($response) {
                    return $response->body();
                })
                ->otherwise(function ($reason) {
                    return json_encode([
                        'success' => false,
                        'message' => $reason->getMessage()
                    ]);
                })->wait(true), true);
    }

    public function single(string $id): array
    {
        return json_decode(
            $this->gateway
                ->get("containers/$id/json")
                ->then(function ($response) {
                    return $response->body();
                })
                ->otherwise(function ($reason) {
                    return json_encode([
                        'success' => false,
                        'message' => $reason->getMessage()
                    ]);
                })->wait(true), true);
    }

    public function create(array $params): array
    {
        $uri = "containers/create";
        if (isset($params['name'])) {
            $uri = $uri . '?' . http_build_query(['name' => $params['name']]);
        }
        return json_decode(
            $this->gateway
                ->post($uri,
                    [
                        'Image' => $params['image']
                    ])
                ->then(function ($response) use ($params) {
                    $this->recordToJournal($params['user'], false);
                    return $response->body();
                })
                ->otherwise(function ($reason) use ($params) {
                    $this->recordToJournal($params['user'], true);
                    return json_encode([
                        'success' => false,
                        'message' => $reason->getMessage()
                    ]);
                })->wait(true), true);
    }

    private function createMessage(int $userId, bool $failed = false): string
    {
        $user = User::findOrFail($userId);
        return $failed ?
            "User {$user->name} failed to create new container" :
            "User {$user->name} created new container";
    }

    private function recordToJournal(int $userId, bool $failure): void
    {
        $record = new Journal(['message' => $this->createMessage($userId, $failure)]);
        $record->save();
    }

    public function start(string $id)
    {
        return json_decode(
            $this->gateway
                ->post("containers/$id/start")
                ->then(function ($response) {
                    return $response->body();
                })
                ->otherwise(function ($reason) {
                    return json_encode([
                        'success' => false,
                        'message' => $reason->getMessage()
                    ]);
                })->wait(true), true);
    }

    public function stop(string $id)
    {
        return json_decode(
            $this->gateway
                ->post("containers/$id/stop")
                ->then(function ($response) {
                    return $response->body();
                })
                ->otherwise(function ($reason) {
                    return json_encode([
                        'success' => false,
                        'message' => $reason->getMessage()
                    ]);
                })->wait(true), true);
    }

    public function pause(string $id, $paused = false)
    {
        $action = $paused ? 'unpause' : 'pause';
        return json_decode(
            $this->gateway
                ->post("containers/$id/$action")
                ->then(function ($response) {
                    return $response->body();
                })
                ->otherwise(function ($reason) {
                    return json_encode([
                        'success' => false,
                        'message' => $reason->getMessage()
                    ]);
                })->wait(true), true);
    }

    public function delete(string $id)
    {
        return json_decode(
            $this->gateway
                ->delete("containers/$id")
                ->then(function ($response) {
                    return $response->body();
                })
                ->otherwise(function ($reason) {
                    return json_encode([
                        'success' => false,
                        'message' => $reason->getMessage()
                    ]);
                })->wait(true), true);
    }

}
