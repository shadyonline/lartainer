<?php

namespace App\Console\Commands;

use Aws\Credentials\CredentialProvider;
use Aws\Sqs\SqsClient;
use Illuminate\Console\Command;

class CreateSQS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:sqs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Init new SQS queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $provider = CredentialProvider::defaultProvider();
        $client = new SqsClient([
            'profile' => 'default',
            'region' => 'us-east-1',
            'version' => '2012-11-05',
            'endpoint' => 'http://sqs:4568',
            'credentials.ini' => $provider
        ]);
        $result = $client->createQueue([
            'Attributes' => [
                'ApproximateReceiveCount' => 5,
                'DelaySeconds' => 5,
                'maxReceiveCount' => 5
            ],
            'QueueName' => 'default',
            'tags' => []
        ]);
        if($result) {
            return 0;
        }
        return 1;
    }
}
