<?php

namespace App\Http\Controllers;

use App\Jobs\CreateContainer;
use App\Services\ContainerService;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use function response;

class ContainerController extends Controller
{
    /**
     * @var mixed
     */
    private $docker;

    /**
     * @throws BindingResolutionException
     */
    public function __construct()
    {
        $this->docker = app()->make(ContainerService::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json($this->docker->list());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {

        dispatch(new CreateContainer(
            $this->docker->create(
                array_merge_recursive(
                    json_decode(
                        $request->getContent(), true
                    ),
                    ['user' => $request->user()->id]
                )
            )
        ));
        return response()->json(['success' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        return response()->json($this->docker->single($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function destroy(string $id): JsonResponse
    {
        return response()->json($this->docker->delete($id));
    }

    public function start(string $id): JsonResponse
    {
        $container = $this->docker->single($id);
        if($container['State']['Paused']) {
            $this->docker->pause($id, true);
        }
        return response()->json($this->docker->start($id));
    }

    public function stop(string $id): JsonResponse
    {
        return response()->json($this->docker->stop($id));
    }

    public function pause(string $id): JsonResponse
    {
        $container = $this->docker->single($id);
        $paused = $container['State']['Paused'];
        return response()->json($this->docker->pause($id, $paused));
    }
}
