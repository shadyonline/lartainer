<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div id="dashboard">
        <router-view name="containerIndex"></router-view>
        <router-view name="containerCreate"></router-view>
        <router-link class="nav-link" to="/create">
            <button class="m-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                Create
            </button>
        </router-link>
        <router-link class="nav-link" to="/">
            <button class="m-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                List
            </button>
        </router-link>
        <a href="/journal">
            <button class="m-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                Journal
            </button>
        </a>
    </div>
</x-app-layout>
