<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Journal') }}
        </h2>
    </x-slot>
    <div id="journal">
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                @foreach(['ID', 'Time', 'Message'] as $head)
                    <th scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        {{ $head }}
                    </th>
                @endforeach
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @foreach($entries as $entry)
                <tr>
                    <td class="px-6 py-4 whitespace-nowrap">
                        {{ $entry['id'] }}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        {{ date('d M Y - H:i:s', strtotime($entry['created_at'])) }}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        {{ $entry['message'] }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</x-app-layout>
