import Vue from 'vue';
import Router from 'vue-router'

require('./bootstrap');

require('alpinejs');

Vue.use(Router);

import ContainersList from './components/pages/ContainersList.vue';
import ContainerCreate from "./components/pages/ContainerCreate";

const routes = [
    {
        path: '/',
        components: {
            containerIndex: ContainersList
        }
    },
    {
        path: '/create',
        components: {
            containerCreate: ContainerCreate
        }

    }
]

const router = new Router({ routes })

new Vue({ router }).$mount('#dashboard')
